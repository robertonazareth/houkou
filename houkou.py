# run.py
#
# Copyright 2019 Roberto Nazareth
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# This file should be used if you don't want to build with meson
# however, we still need to use glib-compile-resources to build the resources file

import os
import sys
import signal
import gettext

VERSION = '0.0.1'

signal.signal(signal.SIGINT, signal.SIG_DFL)
gettext.install('houkou-manga-reader', os.path.join(os.getcwd(), 'src'))

if __name__ == '__main__':
    import gi

    from gi.repository import Gio
    resource = Gio.Resource.load(os.path.join(os.getcwd(), 'houkou-manga-reader.gresource'))
    resource._register()

    from src import main
    sys.exit(main.main(VERSION))

