# files.py
#
# Copyright 2018 Roberto Nazareth <nazarethroberto97@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import os
import pathlib
import uuid
import shutil
import appdirs
import libarchive

# pylint: disable=W0614
from ctypes import *
from ctypes.util import find_library
from gi.repository import GdkPixbuf


class File:
    def __init__(self, filename, data = None):
        self.name = filename
        self.data = data

    def __len__(self) -> int:
        if self.data is None:
            return 0

        return len(self.data)

    def to_pixbuf(self) -> GdkPixbuf.Pixbuf:
        """If possible, transforms the file into a pixbuf. 
        Only workls if with files that can be read as an image"""
        loader = GdkPixbuf.PixbufLoader()
        loader.write(self.data)
        pixbuf = loader.get_pixbuf()
        loader.close()
        return pixbuf


class ar_stream_s(Structure):
    """Used to represent the the 'ar_stream_s' C struct from libunarr."""
    pass


class ar_archive_s(Structure):
    """Used to represent the the 'ar_archive_s' C struct from libunarr."""
    pass


def extract_from_archive(archive_path: str) -> [File]:
    """Extracts the contents of an archive and then returns an array with
    the content data. All archives are extracted using libarchive, but For
    RAR files, we'll use unarr if libarchive fails."""
    path = archive_path.lower() # lower case version of the path
    if path.endswith('.rar') or path.endswith('.cbr'):
        # libarchive doesn't support all RAR features, so if it fails,
        # we'll try to extract using libunarr
        try:
            return extract_using_libarchive(archive_path)
        except:
            pass

        return extract_using_libunarr(archive_path)
    else:
        return extract_using_libarchive(archive_path)


def extract_using_libarchive(archive_path: str) -> [File]:
    """Extracts the contents of an archive and then deletes that folder."""
    # gets the current directory so we can go back to it when we're done
    prev_dir = os.getcwd()
    # gets the application's data directory
    data_dir = get_data_dir()
    #get a random folder name
    folder_name = 'tmp_{}'.format(uuid.uuid4().hex)
    tmp_dir = os.path.join(data_dir, folder_name)

    # create the directory if it doesn't exist
    pathlib.Path(tmp_dir).mkdir(parents=True, exist_ok=True)
    # change to the directory where we will extract the contents of the archive
    os.chdir(tmp_dir)

    try:
        # extract the contents of the file to the current directory
        libarchive.extract_file(archive_path)
    except:
        # go back to the previous directory
        os.chdir(prev_dir)
        # delete the temporary folter
        shutil.rmtree(tmp_dir)
        raise

    # go back to the previous directory
    os.chdir(prev_dir)

    # get a list with all of the files inside the folder
    file_list = list_dir_files(tmp_dir)
    # sort the file list alphabetically
    file_list = sorted(file_list, key=str.lower)
    
    files = []
    for filename in file_list:
        with open(os.path.join(tmp_dir, filename), 'rb') as binary_file:
            data = bytearray(binary_file.read())
            file = File(filename, data)
            files.append(file)

    # deletes the temporary folder
    shutil.rmtree(tmp_dir)
    return files


def extract_using_libunarr(archive_path: str):
    """Uses the unarr library to extract RAR files."""
    # check if the unarr library is avaliable
    libary_path = find_library('unarr')
    if libary_path is None:
        raise Exception

    # link to the library
    cdll.LoadLibrary(libary_path)
    unarr = CDLL(libary_path, use_errno=True)

    # here, we declare how the C functions we're going to use work

    ar_open_file = unarr.ar_open_file
    ar_open_file.restype = POINTER(ar_stream_s)

    ar_open_rar_archive = unarr.ar_open_rar_archive
    ar_open_rar_archive.argtypes = [POINTER(ar_stream_s)]
    ar_open_rar_archive.restype = POINTER(ar_archive_s)

    ar_parse_entry = unarr.ar_parse_entry
    ar_parse_entry.argtypes = [POINTER(ar_archive_s)]
    ar_parse_entry.restype = c_bool

    ar_entry_get_name = unarr.ar_entry_get_name
    ar_entry_get_name.argtypes = [POINTER(ar_archive_s)]
    ar_entry_get_name.restype = c_char_p

    ar_entry_get_size = unarr.ar_entry_get_size
    ar_entry_get_size.argtypes = [POINTER(ar_archive_s)]
    ar_entry_get_size.restype = c_size_t

    ar_entry_uncompress = unarr.ar_entry_uncompress
    ar_entry_uncompress.argtypes = [POINTER(ar_archive_s), c_void_p, c_size_t]
    ar_entry_uncompress.restype = c_bool

    ar_close_archive = unarr.ar_close_archive

    ar_close = unarr.ar_close

    # open file stream
    stream = ar_open_file(archive_path.encode('utf-8'))
    if not stream:
        raise Exception

    # open archive
    archive = ar_open_rar_archive(stream)
    if not archive:
        ar_close(stream)
        raise Exception

    files = []
    while ar_parse_entry(archive):
        # get the name of the file name and data
        filename = ar_entry_get_name(archive).decode('utf-8')
        size = ar_entry_get_size(archive)
        buf = (c_void_p * size)()
        ar_entry_uncompress(archive, buf, size)

        files.append(File(filename, bytearray(buf)))

    ar_close_archive(archive)
    ar_close(stream)

    # sort files alphabetically
    files = sorted(files, key=lambda f: f.name)
    return files


def get_data_dir() -> str:
    """Creates the application's data directory if it doesn't exist and returns its path."""
    path = os.path.join(appdirs.user_data_dir(), 'houkou')
    # create the directory if it doesn't exist
    pathlib.Path(path).mkdir(parents=True, exist_ok=True)

    return path


def get_database_dir() -> str:
    path = os.path.join(get_data_dir(), 'sqlite')
    # create the directory if it doesn't exist
    pathlib.Path(path).mkdir(parents=True, exist_ok=True)

    return path


def list_dir_files(dir_path: str) -> [str]:
    "Returns an array with the name of all of the files on a directory."
    files = []
    for (top_dir, _, filenames) in os.walk(dir_path):
        for i, filename in enumerate(filenames):
            filenames[i] = os.path.join(top_dir, filename)

        files.extend(filenames)

    return files
