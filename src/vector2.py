# vector2.py
#
# Copyright 2018 Roberto Nazareth <nazarethroberto97@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations
from typing import Union


class Vector2:
    def __init__(self, x: float = 0.0, y: float = 0.0):
        self.x = float(x)
        self.y = float(y)
    
    def __add__(self, other: Vector2):
        return Vector2(self.x + other.x, self.y + other.y)

    def __sub__(self, other: Vector2):
        return Vector2(self.x - other.x, self.y - other.y)

    def __mul__(self, other: Union[Vector2, float]) -> Vector2:
        if isinstance(other, Vector2):
            # if 'other' is a Vector2
            return Vector2(self.x * other.x, self.y * other.y)
        else:
            # 'other' can also be a number
            return Vector2(self.x * float(other), self.y * float(other))

    def __div__(self, other: Union[Vector2, float]) -> Vector2:
        if isinstance(other, Vector2):
            # if 'other' is a Vector2
            return Vector2(self.x / other.x, self.y / other.y)
        else:
            # 'other' can also be a number
            return Vector2(self.x / float(other), self.y * float(other))
