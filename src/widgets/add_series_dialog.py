# add_series_dialog.py
#
# Copyright 2018 Roberto Nazareth <nazarethroberto97@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from ..models import Series

@Gtk.Template(resource_path='/org/manga_reader/Houkou-Manga-Reader/add_series_dialog.ui')
class AddSeriesDialog(Gtk.Dialog):
    __gtype_name__ = 'add_series_dialog'

    entry_series_name = Gtk.Template.Child()

    def __init__(self, window: Gtk.Window):
        super().__init__(self)
        self.set_transient_for(window)
        self.set_title('Add new series')

    @Gtk.Template.Callback()
    def _btn_cancel_clicked(self, _widget):
        self.destroy()
    
    # pylint: disable=E1101
    @Gtk.Template.Callback()
    def _btn_ok_clicked(self, _widget):
        name = self.entry_series_name.get_text()
        Series.add(name)
        self.destroy()
