# main_window.py
#
# Copyright 2018 Roberto Nazareth <nazarethroberto97@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from pathlib import Path
from gi.repository import Gdk
from gi.repository import Gio
from gi.repository import Gtk
from ..files import extract_from_archive
from ..models import create_tables, db_path
from .page_reader import PageReader
from .add_chapter_dialog import AddChapterDialod
from .add_series_dialog import AddSeriesDialog


class MainWindow:
    def __init__(self, application):
        self.builder = Gtk.Builder()
        self.builder.add_from_resource('/org/manga_reader/Houkou-Manga-Reader/window.ui')
        self._window = self.builder.get_object('app_window')
        self.headerbar = self.builder.get_object('header_bar')
        self.about_menu = self.builder.get_object('about_menu')
        self.open_menu = self.builder.get_object('open_menu')
        self.new_chapter_menu = self.builder.get_object('new_chapter_menu')
        self.new_series_menu = self.builder.get_object('new_series_menu')
        self.stack = self.builder.get_object('stack')
        self.page_reader = PageReader(self.builder)

        self._window.set_application(application)
        self._connect_signals()
    
    def present(self):
        create_tables()
        self._window.show_all()

    def _connect_signals(self):
        #self._window.connect('destroy', Gtk.main_quit)
        self.open_menu.connect('activate', self._open_menu_clicked)
        self.about_menu.connect('activate', self._about_menu_activate)
        self.new_chapter_menu.connect('activate', self._new_chapter_menu_activate)
        self.new_series_menu.connect('activate', self._new_series_menu_activate)
        self.stack.connect('notify::visible-child-name', self._notify_stack_change)
        self._window.connect('focus-out-event', self._focus_out)

    def _open_menu_clicked(self, _btn):
        dialog = Gtk.FileChooserDialog(
                    "Please choose a file",
                    self._window,
                    Gtk.FileChooserAction.OPEN,
                    (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                    Gtk.STOCK_OPEN, Gtk.ResponseType.OK))

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            files = []
            try:
                files = extract_from_archive(dialog.get_filename())
            except Exception as e:
                print('Failed to open file:\n{}'.format(str(e)))
                self.headerbar.set_subtitle('')
                self.page_reader.remove_all_pages()
                dialog.destroy()
                return
            
            self.page_reader.remove_all_pages()
            self.page_reader.load_pages(files, dialog.get_filename())
            #change the window subtitle to the name of the file
            self.headerbar.set_subtitle(Path(dialog.get_filename()).stem)
            dialog.destroy()
        else:
            dialog.destroy()

    def _about_menu_activate(self, _):
        dialog_builder = Gtk.Builder()
        dialog_builder.add_from_resource('/org/manga_reader/Houkou-Manga-Reader/about.ui')
        about_dialog = dialog_builder.get_object('about_dialog')
        about_dialog.set_modal(True)
        about_dialog.set_transient_for(self._window)
        about_dialog.show()

    def _new_chapter_menu_activate(self, _):
        current_file = self.page_reader.current_file_path
        
        if len(self.page_reader.pages) < 1 or current_file is None:
            return

        try:
            dialog = AddChapterDialod(self._window, current_file)
            dialog.run()
            dialog.destroy()
        except:
            pass 
    
    def _new_series_menu_activate(self, _widget):
        try:
            dialog = AddSeriesDialog(self._window)
            dialog.run()
            dialog.destroy()
        except:
            pass

    def _notify_stack_change(self, _):
        print('the stack was changed')

    def _focus_out(self, _widget, _event):
        self.page_reader.is_alt_key_pressed = False
