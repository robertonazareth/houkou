# page_reader.py
#
# Copyright 2018 Roberto Nazareth <nazarethroberto97@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from functools import reduce
from gi.repository import Gdk
from gi.repository import GdkPixbuf
from gi.repository import Gtk

from ..vector2 import Vector2
from ..files import File
from .page_thumbnail import PageThumbnail


class PageReader:
    def __init__(self, builder: Gtk.Builder):
        self.builder = builder
        self.is_alt_key_pressed = False
        self.root = self.builder.get_object('reading_page')
        self.list_revealer = self.builder.get_object('list_revealer')
        self.page_list = self.builder.get_object('page_list')
        self.page_list_scroll = self.builder.get_object('page_list_scrolled_window')
        self.drawing_area = self.builder.get_object('drawing_area')
        self.scrolled_window = self.builder.get_object('scrolled_window_page_reader')
        self.btn_go_back = self.builder.get_object('btn_go_back')
        self.btn_go_forward = self.builder.get_object('btn_go_forward')
        self.label_page_indicator = self.builder.get_object('label_page_indicator')
        self.btn_zoom_out = self.builder.get_object('btn_zoom_out')
        self.btn_zoom_in = self.builder.get_object('btn_zoom_in')
        self.btn_info = self.builder.get_object('btn_info')
        self._pages = []
        self._current_page = 0
        self._average_page_width = 0.0
        self._page_pos = Vector2(0.0, 0.0)
        self._is_page_spread = False
        # the zoom level that will be used on regular pages
        self._default_zoom_level = 1.0
        # the zoom level that will be used on souble-page spreads   
        self._page_spread_zoom_level = 0.8
        self._previous_click = None
        self._pixbuf_cache = {}
        # string that holds the path of loaded archives
        self.current_file_path = None
        self.ZOOM_LEVEL_DEFAULT = 1.0
        self.ZOOM_LEVEL_STEP = 0.2
        self.ZOOM_LEVEL_SCROLL_STEP = 0.1
        self.ZOOM_LEVEL_MIN = 0.2

        self._connect_signals()

    def _connect_signals(self):
        self.drawing_area.add_events(Gdk.EventMask.BUTTON_PRESS_MASK)
        self.drawing_area.add_events(Gdk.EventMask.BUTTON_RELEASE_MASK)
        self.drawing_area.add_events(Gdk.EventMask.POINTER_MOTION_MASK)

        self.drawing_area.connect('draw', self._drawing_area_draw)
        self.drawing_area.connect('button_press_event', self._drawing_area_pressed)
        self.drawing_area.connect('button_release_event', self._drawing_area_release)
        self.drawing_area.connect('motion_notify_event', self._drawing_area_motion_notify)
        self.btn_zoom_out.connect('clicked', self._btn_zoom_out_clicked)
        self.btn_zoom_in.connect('clicked', self._btn_zoom_in_clicked)
        self.btn_go_back.connect('clicked', self._btn_go_back_clicked)
        self.btn_go_forward.connect('clicked', self._btn_go_forward_clicked)
        self.btn_info.connect('clicked', self._btn_info_clicked)
        self.page_list.connect('row-selected', self._page_list_row_activated)
        self.root.connect('key_press_event', self._key_press)
        self.root.connect('key-release-event', self._key_release)
        self.scrolled_window.connect('scroll-event', self._page_scroll)
        self.scrolled_window.connect('edge-overshot', self._page_scroll_edge_overshot)

    @property
    def default_zoom_level(self) -> float:
        return self._default_zoom_level
    
    @default_zoom_level.setter
    def default_zoom_level(self, zoom_level: float):
        if zoom_level < self.ZOOM_LEVEL_MIN:
            zoom_level = self.ZOOM_LEVEL_MIN
        
        self._default_zoom_level = float(zoom_level)

        try:
            # we need to update the requested size of the drawing area brefore
            # redrawing it, or else this will look kinda clunky
            page_pixbuf = self._get_page(self._current_page)
            requested_width = page_pixbuf.get_width() * self.default_zoom_level
            requested_height =page_pixbuf.get_height() * self.default_zoom_level
            self.drawing_area.set_size_request(requested_width, requested_height)
        except:
            print('file is not a valid image')
        
        # the scrolled window must be redrawn before the drawing area
        self.scrolled_window.queue_draw()
        self.drawing_area.queue_draw()
    
    @property
    def page_spread_zoom_level(self) -> float:
        return self._page_spread_zoom_level
    
    @page_spread_zoom_level.setter
    def page_spread_zoom_level(self, zoom_level: float):
        if zoom_level < self.ZOOM_LEVEL_MIN:
            zoom_level = self.ZOOM_LEVEL_MIN
        
        self._page_spread_zoom_level = float(zoom_level)

        try:
            # we need to update the requested size of the drawing area brefore
            # redrawing it, or else this will look kinda clunky
            page_pixbuf = self._get_page(self._current_page)
            requested_width = float(page_pixbuf.get_width()) * self.page_spread_zoom_level
            requested_height = float(page_pixbuf.get_height()) * self.page_spread_zoom_level
            self.drawing_area.set_size_request(requested_width, requested_height)
        except:
            print('file is not a valid image')
        
        # the scrolled window must be redrawn before the drawing area
        self.scrolled_window.queue_draw()
        self.drawing_area.queue_draw()
    
    @property
    def pages(self):
        return self._pages

    def load_pages(self, pages: [File], archive_path: str):
        self._current_page = 0
        self._pixbuf_cache = {}
        
        self._pages = []
        page_width_list = [] # a list containing the width of every page
        for page in pages:
            # only files that can be read as images will be loaded
            try:
                pixbuf = page.to_pixbuf()
                page_width_list.append(pixbuf.get_width())
                self.page_list.add(PageThumbnail(pixbuf))
                self._pages.append(page)
            except:
                pass
        
        # make all rows unable to grab focus
        for row in self.page_list.get_children():
            row.set_can_focus(False)

        # calculates the average page width
        self._average_page_width = self.calculate_average(page_width_list)

        try:
            # select the first thumbnail, if possible
            thumbail = self.page_list.get_children()[0]
            self.page_list.select_row(thumbail)
        except:
            pass
        
        self.current_file_path = archive_path
        self.page_list.show_all()        
        self.update_page_indicator()
        self.drawing_area.queue_draw()
        self.scrolled_window.grab_focus()
    
    def remove_all_pages(self):
        self._pages = []
        self._current_page = 0
        self._pixbuf_cache = {}
        self.remove_page_thumbnails()
        self.update_page_indicator()
        self.drawing_area.queue_draw()

    def remove_page_thumbnails(self):
        for thumbnail in self.page_list.get_children():
            self.page_list.remove(thumbnail)

        # resets the scroll position        
        self.page_list_scroll.get_hadjustment().set_value(0.0)
        self.page_list_scroll.get_vadjustment().set_value(0.0)

    def update_page_indicator(self):
        current_page = str(self._current_page + 1)
        if len(self._pages) < 1:
            current_page = '0'

        label_text = '{} / {}'.format(current_page, len(self._pages))
        self.label_page_indicator.set_text(label_text)

    def change_cursor_theme(self, cursor_name: str):
        window = self.root.get_window()
        cursor = Gdk.Cursor.new_from_name(window.get_display(), cursor_name)
        window.set_cursor(cursor)

    def go_to_previous_page(self):
        try:
            self.go_to_page(self._current_page - 1, True)
        except:
            pass

    def go_to_next_page(self):
        try:
            self.go_to_page(self._current_page + 1, True)
        except:
            pass
    
    def go_to_page(self, index, update_list_pos = False):
        # check if index isn't out of range
        if index >= len(self._pages) or index < 0:
            raise IndexError

        self._remove_page_from_cache(self._current_page)
        self._current_page = index

        # reset the position of the scrollbars
        self._reset_scrollbar_position()

        try:
            # select the current page on the thumbnail list
            index = self.page_list.get_row_at_index(self._current_page)
            self.page_list.select_row(index)

            if update_list_pos:
                # adjust the position of the thumbnail list's scrollbar
                size, _ = index.get_allocated_size()
                new_x_pos = size.height * self._current_page
                self.page_list_scroll.get_vadjustment().set_value(new_x_pos)
        except:
            pass

        # update widgets
        self.drawing_area.queue_draw()
        self.scrolled_window.grab_focus()
        self.update_page_indicator()

    def calculate_average(self, list: [float]) -> float:
        """Calculates the average of a list of numbers"""
        if len(list) < 1:
            return 0.0

        return reduce(lambda a, b: a + b, list) / len(list)

    def _center_page(self, canvas_width: float, image_width: float):
        """Used to center an image based on its width, the canvas width, and the zoom level."""
        zoom_level = self.default_zoom_level
        if self._is_page_spread:
            zoom_level = self.page_spread_zoom_level

        self._page_pos.x = (float(canvas_width)/2.0)/zoom_level - float(image_width)/2.0

    def _get_page(self, index: int) -> GdkPixbuf.Pixbuf:
        """Creates and caches a pixbuf from the corresponding page. 
        If the pixbuf is already cached, all we do is return its value."""
        # create the hashmap key if it doesn't exist
        if index not in self._pixbuf_cache:
            self._pixbuf_cache.setdefault(index, None)
        
        if isinstance(self._pixbuf_cache[index], GdkPixbuf.Pixbuf):
            return self._pixbuf_cache[index]
        else:
            self._pixbuf_cache[index] = self._pages[index].to_pixbuf()
            return self._pixbuf_cache[index]

    def _remove_page_from_cache(self, i: int):
        self._pixbuf_cache[i] = None

    def _reset_scrollbar_position(self):
        self.scrolled_window.get_hadjustment().set_value(0.0)
        self.scrolled_window.get_vadjustment().set_value(0.0)

    def _drawing_area_draw(self, _, context):
        canvas_width = self.drawing_area.get_allocated_width()
        canvas_height = self.drawing_area.get_allocated_height()
        
        # renders a background on the drawing area based on the current gtk theme
        ctx_style = self.drawing_area.get_style_context()
        Gtk.render_background(ctx_style, context, 0.0, 0.0, canvas_width, canvas_height)

        # be sure that the current page is not bigger than the pages array nor lower than 0
        if self._current_page > len(self._pages):
            self._current_page = len(self._pages) - 1
        elif self._current_page < 0:
            self._current_page = 0

        # stop the function if no pages are loaded
        if len(self._pages) <= 0:
            return False

        # if possible, transform the current page into a pixbuf
        try:
            # gets the current page's pixbuf
            page_pixbuf = self._get_page(self._current_page)

            # detect double-page spreads
            if page_pixbuf.get_width() > self._average_page_width * 1.2:
                self._is_page_spread = True
            else:
                self._is_page_spread = False

            zoom_level = self.default_zoom_level
            if self._is_page_spread:
                zoom_level = self._page_spread_zoom_level

            requested_width = page_pixbuf.get_width() * zoom_level
            requested_height = page_pixbuf.get_height() * zoom_level
            self.drawing_area.set_size_request(requested_width, requested_height)

            self._center_page(canvas_width, page_pixbuf.get_width())
            context.scale(zoom_level, zoom_level)
            Gdk.cairo_set_source_pixbuf(context, page_pixbuf, self._page_pos.x, self._page_pos.y)
            context.paint()

            return False
        except:
            return False

    def _drawing_area_pressed(self, _, event):
        self._previous_click = Vector2(event.x, event.y)
        self.scrolled_window.grab_focus()
        return False
    
    def _drawing_area_release(self, _, event):
        self._previous_click = None
        self.change_cursor_theme('default')
        return False

    def _drawing_area_motion_notify(self, _, event):
        """Event that notifies us every time the mouse cursor moves."""
        # if the cursor is dragging the drawing area
        if event.get_state() == Gdk.ModifierType.BUTTON1_MASK | Gdk.ModifierType.MOD2_MASK:
            self.change_cursor_theme('grabbing')
            h_scrollbar = self.scrolled_window.get_hadjustment()
            v_scrollbar = self.scrolled_window.get_vadjustment()
            
            if isinstance(self._previous_click, Vector2):
                current_click = Vector2(event.x, event.y)
                difference = self._previous_click - current_click
                h_scrollbar.set_value(h_scrollbar.get_value() + difference.x)
                v_scrollbar.set_value(v_scrollbar.get_value() + difference.y)

                relative_click = current_click + difference
                self._previous_click = relative_click
                self.drawing_area.queue_draw()
        else:
            self._previous_click = None
            self.change_cursor_theme('default')

        return False

    def _btn_zoom_out_clicked(self, _):
        if self._is_page_spread:
            self.page_spread_zoom_level -= self.ZOOM_LEVEL_STEP
        else:
            self.default_zoom_level -= self.ZOOM_LEVEL_STEP

    def _btn_zoom_in_clicked(self, _):
        if self._is_page_spread:
            self.page_spread_zoom_level += self.ZOOM_LEVEL_STEP
        else:
            self.default_zoom_level += self.ZOOM_LEVEL_STEP
    
    def _btn_go_back_clicked(self, _):
        self.go_to_previous_page()            

    def _btn_go_forward_clicked(self, _):
        self.go_to_next_page()
        
    def _btn_info_clicked(self, _):
        # close the side panel if it's opened, open it if it's closed
        self.list_revealer.set_reveal_child(not self.list_revealer.get_reveal_child())
    
    def _page_list_row_activated(self, _, activated_row):
        try:
            self.go_to_page(activated_row.get_index())
            self.drawing_area.grab_focus()
        except:
            pass
    
    def _key_press(self, _, event):
        """This event notifies us every time a key is pressed."""
        if event.keyval == Gdk.KEY_Left:
            self.go_to_previous_page()
            return True
        elif event.keyval == Gdk.KEY_Right:
            self.go_to_next_page()
            return True
        elif event.keyval == Gdk.KEY_Up:
            adjustment = self.scrolled_window.get_vadjustment()
            step = adjustment.get_step_increment()
            adjustment.set_value(adjustment.get_value() - step)
            return True
        elif event.keyval == Gdk.KEY_Down:
            adjustment = self.scrolled_window.get_vadjustment()
            step = adjustment.get_step_increment()
            adjustment.set_value(adjustment.get_value() + step)
            return True
        elif event.keyval == Gdk.KEY_Alt_L:
            self.is_alt_key_pressed = True
            return False
        elif event.keyval == Gdk.KEY_space:
            # when the space key is pressed, we scroll down or go to  
            # the next page if we're already at the bottom
            adjustment = self.scrolled_window.get_vadjustment()
            current_pos = adjustment.get_value() 
            max_value = adjustment.get_upper() - adjustment.get_page_size()

            if current_pos == max_value:
                # if we're at the bottom of the page, go to the next one
                if self._current_page < len(self._pages) - 1:
                    # don't do anything if we're at the last page
                    self.go_to_next_page()
            elif adjustment.get_upper() == adjustment.get_page_size():
                # if the page is not big enough to scroll, go to the next one
                self.go_to_next_page()
            else:
                # scroll the page down
                new_pos = current_pos + (adjustment.get_upper()/2.0)
                if new_pos > max_value:
                    new_pos = max_value
                adjustment.set_value(new_pos)        
            
            return True

        return False
                
    def _key_release(self, _, event):
        """This event notifies us every time a key is released."""
        if event.keyval == Gdk.KEY_Alt_L:
            self.is_alt_key_pressed = False
    
    def _page_scroll(self, _, event):
        """This event notifies us every time the page is scrolled."""
        adjustment = self.scrolled_window.get_vadjustment()
        
        if self.is_alt_key_pressed:
            if event.delta_y > 0:
                if self._is_page_spread:
                    self.page_spread_zoom_level -= self.ZOOM_LEVEL_SCROLL_STEP
                else:
                    self.default_zoom_level -= self.ZOOM_LEVEL_SCROLL_STEP
            elif event.delta_y < 0:
                if self._is_page_spread:
                    self.page_spread_zoom_level += self.ZOOM_LEVEL_SCROLL_STEP
                else:
                    self.default_zoom_level += self.ZOOM_LEVEL_SCROLL_STEP

            return True
        elif adjustment.get_upper() == adjustment.get_page_size():
            # if the page isn't big enough to be scrolled, 
            # go to the next or previous page
            if event.delta_y > 0:
                self.go_to_next_page()
            elif event.delta_y < 0:
                self.go_to_previous_page() 
        
        return False 
    
    def _page_scroll_edge_overshot(self, _widget, pos):
        """Goes to the next or previous page when the scrollbar is overshoted."""
        if pos == Gtk.PositionType.BOTTOM:
            # don't do anything if we're on the last page
            if self._current_page < len(self._pages) - 1:
                self.go_to_next_page()
        elif pos == Gtk.PositionType.TOP:
            # don't do anything if we're on the first page
            if self._current_page > 0:
                self.go_to_previous_page()
            
                # scroll to the bottom of the page
                adjustment = self.scrolled_window.get_vadjustment()
                adjustment.set_value(adjustment.get_upper())
