# page_thumbnail.py
#
# Copyright 2018 Roberto Nazareth <nazarethroberto97@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from pathlib import Path
from gi.repository import GdkPixbuf
from gi.repository import Gtk


@Gtk.Template(resource_path='/org/manga_reader/Houkou-Manga-Reader/page_thumbnail.ui')
class PageThumbnail(Gtk.Box):
    __gtype_name__ = 'page_thumbnail'

    _image_thumbnail = Gtk.Template.Child()

    # pylint: disable=E1101
    def __init__(self, pixbuf):
        super().__init__(self)
        iterp = GdkPixbuf.InterpType.BILINEAR
        scaled_pixbuf = pixbuf.scale_simple(125, 150, iterp)
        self._image_thumbnail.set_from_pixbuf(scaled_pixbuf)
