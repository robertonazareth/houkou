from __future__ import annotations

import datetime
import os
# pylint: disable=W0614
from peewee import *
from .files import get_database_dir

db_path = os.path.join(get_database_dir(), 'houkou.db')
db = SqliteDatabase(db_path)


class BaseModel(Model):
    class Meta:
        database = db


class Series(BaseModel):
    name = CharField(default='', null=False)
    cover_path = CharField(null=True)
    created_at = DateTimeField(default=datetime.datetime.now, null=False)
    changed_at = DateTimeField(default=datetime.datetime.now, null=False)

    @staticmethod
    def add(name: str, cover_path: str = None) -> Series:
        db.connect()
        series = Series(name=name, cover_path=cover_path)
        series.save()
        db.close()
        return series
    
    @staticmethod
    def find_all() -> [Series]:
        db.connect()
        series_list = Series.select()
        db.close()
        return series_list


class Chapter(BaseModel):
    series = ForeignKeyField(Series, backref='chapters', null=True)
    name = CharField(default='', null=False)
    file_path = CharField(null=False)
    cover_path = CharField(null=True)
    is_read = BooleanField(default=False)
    created_at = DateTimeField(default=datetime.datetime.now, null=False)
    changed_at = DateTimeField(default=datetime.datetime.now, null=False)

    @staticmethod
    def add(name: str, path: str) -> Chapter:
        db.connect()
        chapter = Chapter(name=name, file_path=path)
        chapter.save()
        db.close()
        return chapter
    
    @staticmethod
    def find_all() -> [Chapter]:
        db.connect()
        chapters = Chapter.select()
        db.close()
        return chapters


def create_tables():
    db.connect()
    db.create_tables([Series, Chapter])
    db.close()