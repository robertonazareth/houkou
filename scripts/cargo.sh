#!/bin/sh

export CARGO_HOME=$1/target/cargo-home

if [[ $DEBUG = true ]]
then
    echo "DEBUG MODE"
    cargo build && cp $1/target/debug/houkou-manga-reader $2
else
    echo "RELEASE MODE"
    cargo build --release && cp $1/target/release/houkou-manga-reader $2
fi 